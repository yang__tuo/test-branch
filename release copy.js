/* eslint-disable global-require */
const fs = require('fs');
const readline = require('readline');
const child = require('child_process');

function readSyncByRl() {
  const tips = '1: 主版本号\n2: 次版本号\n3: 补丁版本号\n4: 尾版本号\n请输入tag模式：>3?';
  return new Promise(async resolve => {
    let rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    const cb = answer => {
      rl.close();
      const input = answer.trim() || '3';
      if (['1', '2', '3', '4'].includes(input)) {
        resolve(input);
      } else {
        process.stdout.write('\n');
        rl = readline.createInterface({
          input: process.stdin,
          output: process.stdout,
        });
        rl.question(tips, cb);
      }
    };

    rl.question(tips, cb);
  }).then(input => {
    const packageInfo = require('./package.json');
    const versionReg = /\d+/g;
    const oldVersion = packageInfo.version;
    const versionNums = oldVersion.match(versionReg);
    switch (input) {
      case '1':
        versionNums[0]++;
        break;
      case '2':
        versionNums[1]++;
        break;
      case '3':
        versionNums[2]++;
        break;
      case '4':
        versionNums[versionNums.length - 1]++;
        break;
      default:
        versionNums[2]++;
        break;
    }
    let newVersion = versionNums.slice(0, 3).join('.');
    if (versionNums.length === 4) {
      newVersion = `${newVersion}-${versionNums[3]}`;
    }
    packageInfo.version = newVersion;
    fs.writeFileSync('./package.json', JSON.stringify(packageInfo, undefined, 2), 'utf-8');
    child.execSync(`git add .`);
    child.execSync(
      `git add . && git commit -m "v${newVersion}" && git push && git tag ${newVersion} && git push origin ${newVersion}`
    );
  });
}

readSyncByRl();
