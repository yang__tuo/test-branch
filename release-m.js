const child = require('child_process');
// 升级1.x.0中版本
child.execSync(`git pull && npm version minor && git push`);
// 再读取版本号
const packageInfo = require('./package.json');

const versionNums = packageInfo.version.match(/\d+/g);
const newVersion = versionNums.slice(0, 3).join('.');
// 打tag
child.execSync(`git tag ${newVersion} && git push origin ${newVersion}`);
